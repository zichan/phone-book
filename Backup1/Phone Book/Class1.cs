using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
[XmlRootAttribute("Contact", Namespace = "", IsNullable = false)]
public class Contact
{
    private string LName;
    private string FName;
    private string address;
    private string city;
    private string eMail;
    private string country;
    private string state;
    private string zCode;
    private string hNumber;
    private string wNumber;
    private string mNumber;
    private string fax;
    private string notes;
    private Bitmap picture;
    public Contact()
    {
    }
    [XmlIgnoreAttribute()]
    public Bitmap Picture
    {
        get { return picture; }
        set { picture = value; }
    }
    #region XML Elements and variable thingy's
    [XmlElement("Fax")]
    public String Fax
    {
        get
        {
            return fax;
        }
        set
        {
            fax = value;
        }
    }
    [XmlElement("Country")]
    public String Country
    {
        get
        {
            return country;
        }
        set
        {
            country = value;
        }
    }
    [XmlElement("State")]
    public String State
    {
        get
        {
            return state;
        }
        set
        {
            state = value;
        }
    }
    [XmlElement("Moble Number")]
    public String MobleNumber
    {
        get
        {
            return mNumber;
        }
        set
        {
            mNumber = value;
        }
    }
    [XmlElement("Work Number")]
    public String WorkNumber
    {
        get
        {
            return wNumber;
        }
        set
        {
            wNumber = value;
        }
    }
    [XmlElement("Home Number")]
    public String HomeNumber
    {
        get
        {
            return hNumber;
        }
        set
        {
            hNumber = value;
        }
    }
    [XmlElement("Zip Code")]
    public String ZipCode
    {
        get
        {
            return zCode;
        }
        set
        {
            zCode = value;
        }
    }
    [XmlElement("Email")]
    public String EMail
    {
        get
        {
            return eMail;
        }
        set
        {
            eMail = value;
        }
    }
    [XmlElement("City")]
    public String City
    {
        get
        {
            return city;
        }
        set
        {
            city = value;
        }
    }
    [XmlElement("Address")]
    public String Address
    {
        get
        {
            return address;
        }
        set
        {
            address = value;
        }
    }
    [XmlElement("Last Name")]
    public String LastName
    {
        get
        {
            return LName;
        }
        set
        {
            LName = value;
        }
    }
    [XmlElement("First Name")]
    public String FirstName
    {
        get
        {
            return FName;
        }
        set
        {
            FName = value;
        }
    }
    [XmlElement("Notes")]
    public String Notes
    {
        get
        {
            return notes;
        }
        set
        {
            notes = value;
        }
    }
    [XmlElementAttribute("Picture")]
    public byte[] PictureByteArray
    {
        get
        {
            if (picture != null)
            {
                TypeConverter BitmapConverter = TypeDescriptor.GetConverter(picture.GetType());
                return (byte[])BitmapConverter.ConvertTo(picture, typeof(byte[]));
            }
            else
                return null;
        }

        set
        {
            if (value != null)
                picture = new Bitmap(new MemoryStream(value));
            else
                picture = null;
        }
    }
    #endregion
    public void Save()
    {
        XmlSerializer s = new XmlSerializer(typeof(Contact));
        TextWriter w;
        string Lnamef3 = LName.Substring(0, 3);
        if (Phone_Book.Properties.Settings.Default.SaveMyDocs)
            w = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "/Address Book"+"/" + this.FirstName.ToString() + Lnamef3.ToString() + ".cnt");
        else
            w = new StreamWriter(Phone_Book.Properties.Settings.Default.UserDir + this.FirstName.ToString() + Lnamef3.ToString() + ".cnt");
        s.Serialize(w, this);
        w.Close();
    }
    public void Save(String filename)
    {
        XmlSerializer s = new XmlSerializer(typeof(Contact));
        string Lnamef3 = LName.Substring(0, 3);
        TextWriter w = new StreamWriter(filename + this.FirstName.ToString() + Lnamef3.ToString() + ".cnt");
        s.Serialize(w, this);
        w.Close();
    }
}
