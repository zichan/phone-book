using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
namespace Phone_Book
{
    public partial class ContactView : Form
    {
        String file;
        Boolean frame;
        Contact cnt;
        FileInfo inf;
        public ContactView(String FileName,Boolean onlyFrame)
        {
            frame = onlyFrame;
            file = FileName;
            InitializeComponent();
            openFile();
        }
        public ContactView(String FileName, Boolean onlyFrame, FileInfo fi)
        {
            inf = fi;
            frame = onlyFrame;
            file = FileName;
            InitializeComponent();
            openFile();
        }
        public Contact load(FileInfo file)
        {
            Contact Cnt;
            XmlSerializer s = new XmlSerializer(typeof(Contact));
            TextReader w = new StreamReader(file.ToString());
            Cnt = (Contact)s.Deserialize(w);
            w.Close();
            return Cnt;
        }
        public void openFile()
        {
            FileInfo f = new FileInfo(file);
            cnt = load(f);
            textBox1.Text = cnt.FirstName;
            textBox2.Text = cnt.LastName;
            textBox3.Text = cnt.Address;
            textBox4.Text = cnt.City;
            textBox5.Text = cnt.State;
            textBox6.Text = cnt.ZipCode;
            textBox7.Text = cnt.Country;
            textBox8.Text = cnt.HomeNumber;
            textBox9.Text = cnt.WorkNumber;
            textBox10.Text = cnt.MobleNumber;
            textBox11.Text = cnt.EMail;
            textBox12.Text = cnt.Fax;
            richTextBox1.Text = cnt.Notes;
            if(cnt.Picture!=null)
                pictureBox1.Image = ConvertImage(cnt.Picture, pictureBox1.Width, pictureBox1.Height);
        }
        private Image ConvertImage(Image source, int MaxWidth, int MaxHeight)
        {
            float MaxRatio = MaxWidth / (float)MaxHeight;
            float ImgRatio = source.Width / (float)source.Height;

            if (source.Width > MaxWidth)
                return new Bitmap(source, new Size(MaxWidth, (int)Math.Round(MaxWidth /
                ImgRatio, 0)));

            if (source.Height > MaxHeight)
                return new Bitmap(source, new Size((int)Math.Round(MaxWidth * ImgRatio,
                0), MaxHeight));
            return source;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (frame == true)
                Application.Exit();
            else
                this.Dispose();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            ContData ct = new ContData(cnt, inf);
            ct.Owner = this.Owner;
            this.Dispose();
            ct.ShowDialog(ct.Owner);
        }
        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(textBox3.Text);
        }
        void t_GotFocus(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;
            t.Focus();
        }
    }
}