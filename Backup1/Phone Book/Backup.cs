using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Windows.Forms;
[XmlRootAttribute("BackUp", Namespace = "", IsNullable = false)]
public class Backup
{
    private Contact[] cnts;
    public Backup()
    {
    }
    [XmlArray("Contacts"), XmlArrayItem("Contact", typeof(Contact))]
    public Contact[] Contacts
    {
        get
        {
            return cnts;
        }
        set
        {
            cnts = value;
        }
    }
    /*public void Save()
    {
        XmlSerializer s = new XmlSerializer(typeof(Backup));
        FileStream w=null;
        DateTime dt = new DateTime();
        if (Phone_Book.Properties.Settings.Default.BkpSv == 1)
        {
            w = new FileStream(Application.StartupPath + "/BackUp Files/" + dt.Day.ToString() + dt.Month.ToString() + dt.Year.ToString() + ".bkp",FileMode.Create,FileAccess.ReadWrite);
        }
        else
        {
            FolderBrowserDialog f = new FolderBrowserDialog();
            f.ShowNewFolderButton = true;
            if (f.ShowDialog() == DialogResult.OK)
                w = new FileStream(f.SelectedPath + dt.Day.ToString() + dt.Month.ToString() + dt.Year.ToString() + ".bkp", FileMode.Create, FileAccess.ReadWrite);
        }
        s.Serialize(w,this);
        w.Close();
    }
     * */
}
