using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
namespace Phone_Book
{
    public partial class ContData : Form
    {
        Contact first;
        FileInfo inf;
        #region States and Countrys
        String[] States =
        {
            "ALABAMA","ALASKA","ARIZONA","ARKANSAS","CALIFORNIA","COLORADO",
            "CONNECTICUT","DELAWARE","DISTRICT OF COLUMBIA","FLORIDA","GEORGIA","HAWAII","IDAHO","ILLINOIS",
            "INDIANA","IOWA","KANSAS","KENTUCKY","LOUISIANA","MAINE",
            "MARYLAND","MASSACHUSETTS","MICHIGAN","MINNESOTA","MISSISSIPPI",
            "MISSOURI","  MONTANA","  NEBRASKA","  NEVADA","  NEW HAMPSHIRE","  NEW JERSEY",
            "NEW MEXICO","  NEW YORK","  NORTH CAROLINA","  NORTH DAKOTA","OHIO","  OKLAHOMA",
            "OREGON","PENNSYLVANIA","  PUERTO RICO","  RHODE ISLAND","  SOUTH CAROLINA",
            "SOUTH DAKOTA","  TENNESSEE","  TEXAS","  UTAH","  VERMONT","  VIRGINIA",
            "  VIRGIN ISLANDS","  WASHINGTON ","  WASHINGTON, DC","  WEST VIRGINIA","WISCONSIN","  WYOMING"  
        };
        String[] Countries = 
        {
            "Afghanistan ","Albania","Algeria","American Samoa","Andorra",
            "Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina",
            "Armenia","Arctic Ocean"," Aruba","Ashmore and Cartier Islands",
            "Atlantic Ocean","Australia","Austria","Azerbaijan","Bahamas",
            "Bahrain","Baker Island","Bangladesh","Barbados","Bassas da India",
            "Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia",
            "Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil",
            "British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi",
            "Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands",
            "Central African Republic","Chad","Chile","China","Christmas Island",
            "Clipperton Island","Cocos Islands","Colombia","Comoros","Cook Islands",
            "Coral Sea Islands","Costa Rica","Cote d`Ivoire","Croatia","Cuba",
            "Cyprus","Czech Republic","Denmark","Democratic Republic of the Congo",
            "Djibouti","Dominica","Dominican Republic","East Timor","Ecuador",
            "Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia",
            "Europa Island","Falkland Islands (Islas Malvinas)","Faroe Islands",
            "Fiji","Finland","France","French Guiana","French Polynesia",
            "French Southern and Antarctic Lands","Gabon","Gambia","Gaza Strip",
            " Georgia","Germany","Ghana","Gibraltar","Glorioso Islands","Greece",
            "Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey",
            "Guinea","Guinea-Bissau","Guyana","Haiti","Heard Island and McDonald Islands",
            "Honduras","Hong Kong","Howland Island","Hungary"," Iceland","India",
            "Indian Ocean","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel",
            "Italy","Jamaica","Jan Mayen","Japan","Jarvis Island","Jersey",
            "Johnston Atoll","Jordan","Juan de Nova Island","Kazakhstan","Kenya",
            "Kingman Reef","Kiribati","Kerguelen Archipelago","Kuwait","Kyrgyzstan",
            " Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein",
            "Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi",
            "Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique",
            "Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Midway Islands",
            "Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco",
            "Mozambique","Myanmar","Namibia","Nauru","Navassa Island","Nepal",
            "Netherlands","Netherlands Antilles","New Caledonia","New Zealand",
            "Nicaragua","Niger","Nigeria","Niue","Norfolk Island","North Korea",
            "Northern Mariana Islands","Norway","Oman","Pacific Ocean","Pakistan",
            "Palau","Palmyra Atoll","Panama","Papua New Guinea","Paracel Islands",
            "Paraguay","Peru","Philippines","Pitcairn Islands","Poland","Portugal",
            "Puerto Rico","Qatar","Reunion","Republic of the Congo","Romania","Russia",
            "Rwanda","Saint Helena","Saint Kitts and Nevis","Saint Lucia",
            "Saint Pierre and Miquelon","Saint Vincent and the Grenadines",
            "Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal",
            "Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia",
            "Solomon Islands","Somalia","South Africa","South Georgia and the South Sandwich Islands",
            "South Korea","Spain","Spratly Islands","Sri Lanka","Sudan","Suriname",
            "Svalbard","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan",
            "Tanzania","Thailand","Togo","Tokelau",
            "Tonga","Trinidad and Tobago","Tromelin Island","Tunisia","Turkey",
            "Turkmenistan","Turks and Caicos Islands","Tuvalu","Uganda ","Ukraine ",
            "United Arab Emirates ","United Kingdom ","USA ","Uruguay ","Uzbekistan ","Vanuatu ",
            "Venezuela ","Viet Nam ","Virgin Islands ","Wake Island ","Wallis and Futuna ",
            "West Bank ","Western Sahara ","Yemen ","Yugoslavia ","Zambia","Zimbabwe"
        };
        #endregion
        Boolean Edit = false;
        Boolean CityFound=false;
        public ContData()
        {
            InitializeComponent();
        }
        public ContData(Contact cnt,FileInfo fi)
        {
            Edit = true;
            first = cnt;
            inf = fi;
            InitializeComponent();
            Fname.Text = cnt.FirstName;
            LName.Text = cnt.LastName;
            Address.Text = cnt.Address;
            City.Text = cnt.City;
            State.Text = cnt.State;
            Country.Text = cnt.Country;
            Zip.Text = cnt.ZipCode.ToString();
            Home.Text = cnt.HomeNumber.ToString();
            work.Text = cnt.WorkNumber.ToString();
            Moble.Text = cnt.MobleNumber.ToString();
            Fax.Text = cnt.Fax.ToString();
            Notes.Text = cnt.Notes;
            Email.Text = cnt.EMail;
            if(cnt.Picture!=null)
                pictureBox1.Image = ConvertImage(cnt.Picture, pictureBox1.Width, pictureBox1.Height);
        }
        private void textBox3_MouseClick(object sender, MouseEventArgs e)
        {
            Email.Text = "";
        }
        public Contact SVCNT()
        {
            Contact cnt = new Contact();
            cnt.FirstName = Fname.Text;
            cnt.LastName = LName.Text;
            cnt.Address = Address.Text;
            cnt.City = City.Text;
            cnt.State = State.Text;
            cnt.Country = Country.Text;
            cnt.ZipCode = Zip.Text;
            cnt.HomeNumber = Home.Text;
            cnt.WorkNumber = work.Text;
            cnt.MobleNumber = Moble.Text;
            cnt.Fax = Fax.Text;
            cnt.Notes = Notes.Text;
            cnt.EMail = Email.Text;
            cnt.Picture = (Bitmap)pictureBox1.Image;
            String[] Info = new String[Properties.Settings.Default.Citys.Count];
            Properties.Settings.Default.Citys.CopyTo(Info, 0);
            Boolean Found = false;
            for (int i = 0; i < Info.Length; i++)
            {
                Token tok = new Token(Info[i]);
                if (tok.City == City.Text)
                {
                    Found = true;
                }
            }
            if (!Found)
            {
                String Text = City.Text + "/" + State.Text + "/" + Country.Text + "/" + Zip.Text + "/";
                Properties.Settings.Default.Citys.Add(Text);
                Properties.Settings.Default.Save();
            }
            return cnt;
        }
        private Image ConvertImage(Image source, int MaxWidth, int MaxHeight)
        {
            float MaxRatio = MaxWidth / (float)MaxHeight;
            float ImgRatio = source.Width / (float)source.Height;

            if (source.Width > MaxWidth)
                return new Bitmap(source, new Size(MaxWidth, (int)Math.Round(MaxWidth /
                ImgRatio, 0)));

            if (source.Height > MaxHeight)
                return new Bitmap(source, new Size((int)Math.Round(MaxWidth * ImgRatio,
                0), MaxHeight));

            return source;
        }
        Boolean validData;
        private void pictureBox1_DragDrop(object sender, DragEventArgs e)
        {
            if (validData)
            {
                string[] filepath = (string[])e.Data.GetData(DataFormats.FileDrop);
                Image image = Image.FromFile(filepath[0]);
                pictureBox1.Image = ConvertImage(image, pictureBox1.Width, pictureBox1.Height);
            }
            MessageBox.Show(e.Data.GetData(DataFormats.Bitmap).ToString());
        }
        private void onDragEnter(object sender, DragEventArgs e)
        {
            string filename;
            validData = GetFilename(out filename, e);
            if (validData)
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }

        }
        protected bool GetFilename(out string filename, DragEventArgs e)
        {
            bool ret = false;
            filename = String.Empty;

            if ((e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
            {
                Array data = ((IDataObject)e.Data).GetData("FileDrop") as Array;
                if (data != null)
                {
                    if ((data.Length == 1) && (data.GetValue(0) is String))
                    {
                        filename = ((string[])data)[0];
                        string ext = Path.GetExtension(filename).ToLower();
                        if ((ext == ".jpg") || (ext == ".png") || (ext == ".bmp"))
                        {
                            ret = true;
                        }
                    }
                }
            }
            return ret;
        }
        private void ContData_Load_1(object sender, EventArgs e)
        {
            String[] citys = new String[Properties.Settings.Default.Citys.Count];
            Properties.Settings.Default.Citys.CopyTo(citys,0);
            AutoCompleteStringCollection CiTys = new AutoCompleteStringCollection();
            foreach (string a in citys)
                CiTys.Add(new Token(a).City);
            City.AutoCompleteCustomSource = CiTys;
            AutoCompleteStringCollection StaTes = new AutoCompleteStringCollection();
            foreach (string a in States)
                StaTes.Add(a);
            State.AutoCompleteCustomSource = StaTes;
            AutoCompleteStringCollection CountRys = new AutoCompleteStringCollection();
            foreach (string a in Countries)
                CountRys.Add(a);
            Country.AutoCompleteCustomSource = CountRys;
            if (Edit == true)
            {
                toolStripButton13.Enabled = false;
                toolStripButton15.Enabled = false;
            }
        }
        private void toolStripButton14_Click(object sender, EventArgs e)
        {
            try
            {
                Contact cnt = SVCNT();
                if (inf != null)
                    if (first.FirstName != cnt.FirstName || first.LastName != cnt.LastName)
                        inf.Delete();
                if (Phone_Book.Properties.Settings.Default.SaveSet == 1)
                {
                    FileInfo f = new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "/Address Book" + "/" + cnt.FirstName.ToString() + cnt.LastName.Substring(0, 3) + ".cnt");
                    if (f.Exists&&Edit==false)
                    {
                        if (MessageBox.Show(this, "This Contact Already Exists" + "\n" + "Are You Shure You Want To Replace It?", "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            cnt.Save();
                    }
                    else
                        cnt.Save();
                }
                else if (Phone_Book.Properties.Settings.Default.SaveSet == 2)
                {
                    FileInfo f = new FileInfo(Properties.Settings.Default.CurrentDir + "/");
                    if (f.Exists && Edit == false)
                    {
                        if (MessageBox.Show(this, "This Contact Already Exists" + "\n" + "Are You Shure You Want To Replace It?", "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            cnt.Save(f.FullName);
                    }
                    else
                        cnt.Save(f.FullName);
                }
                Desktop dk = (Desktop)Owner;
                dk.LoadContacts();
                this.Dispose();
            }
            catch (Exception)
            {
                MessageBox.Show(this, "You Must At Least Enter A First name And A Last Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void toolStripButton15_Click(object sender, EventArgs e)
        {
            if(Fname.Text!=""||LName.Text!="")
            {
                if (Folder.ShowDialog(this) == DialogResult.OK)
                {
                    Contact cnt = SVCNT();
                    if (inf != null)
                        if (first.FirstName != cnt.FirstName || first.LastName != cnt.LastName)
                            inf.Delete();
                    FileInfo f = new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "/Address Book" + "/");
                    if (f.Exists)
                    {
                        if (MessageBox.Show(this, "This Contact Already Exists" + "\n" + "Are You Shure You Want To Replace It?", "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            cnt.Save(Folder.SelectedPath);
                    }
                    else
                        cnt.Save(Folder.SelectedPath);
                    Desktop dk = (Desktop)Owner;
                    dk.LoadContacts();
                    this.Dispose();
                }
            }
            else
            {
                MessageBox.Show(this, "You Must At Least Enter A First name And A Last Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void toolStripButton13_Click(object sender, EventArgs e)
        {
            if(Fname.Text!=""||LName.Text!="")
            {
                Contact cnt = SVCNT();
                if (inf != null)
                    if (first.FirstName != cnt.FirstName || first.LastName != cnt.LastName)
                        inf.Delete();
                if (Phone_Book.Properties.Settings.Default.SaveSet == 1)
                {
                    FileInfo f = new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "/Address Book" + "/" + cnt.FirstName.ToString() + cnt.LastName.Substring(0, 3) + ".cnt");
                    if (f.Exists&&Edit==false)
                    {
                        if (MessageBox.Show(this, "This Contact Already Exists" + "\n" + "Are You Shure You Want To Replace It?", "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            cnt.Save();
                    }
                    else
                        cnt.Save();
                }
                else if (Phone_Book.Properties.Settings.Default.SaveSet == 2)
                {
                    FileInfo f = new FileInfo(Properties.Settings.Default.CurrentDir+"/");
                    if (f.Exists&&Edit==false)
                    {
                        if (MessageBox.Show(this, "This Contact Already Exists" + "\n" + "Are You Shure You Want To Replace It?", "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            cnt.Save(f.FullName);
                    }
                    else
                        cnt.Save(f.FullName);
                }
                Desktop dk = (Desktop)Owner;
                dk.LoadContacts();
                new ContData().Show((Desktop)this.Owner);
                if (!Properties.Settings.Default.Citys.Contains(City.Text))
                {
                    Properties.Settings.Default.Citys.Add(City.Text);
                    Properties.Settings.Default.Save();
                }
                this.Dispose();
            }
            else
            {
                MessageBox.Show(this, "You Must At Least Enter A First name And A Last Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void toolStripButton16_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        private void City_KeyUp(object sender, KeyEventArgs e)
        {
            String[] Info = new String[Properties.Settings.Default.Citys.Count];
            Properties.Settings.Default.Citys.CopyTo(Info,0);
            for (int i = 0; i < Info.Length; i++)
            {
                Token tok = new Token(Info[i]);
                if (tok.City == City.Text)
                {
                    State.Text = tok.State;
                    Country.Text = tok.Country;
                    Zip.Text = tok.Zip;
                    CityFound = true;
                }
            }
        }
        private void City_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if(CityFound)
                    Home.Select();
            }
        }
    }
}