using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.IO;
namespace Phone_Book
{
    public class DataGridCellTips : DataGridView
    {
        public DataSet testData;
        public DataTable testTable;
        private int hitRow;
        System.Timers.Timer tim = new System.Timers.Timer();
        private System.Windows.Forms.ToolTip toolTip1;
        Contact[] cnt;
        public DataGridCellTips()
        {
            tim.Interval = 1000;
            tim.AutoReset = false;
            hitRow = -1;
            this.toolTip1 = new System.Windows.Forms.ToolTip();
            toolTip1.IsBalloon = true;
            this.toolTip1.InitialDelay = 1000;
            toolTip1.AutomaticDelay = 2000;
            toolTip1.UseAnimation = true;
            toolTip1.UseFading = true;
            toolTip1.ReshowDelay = 500;
            this.MouseMove += new MouseEventHandler(HandleMouseMove);
            tim.Elapsed += new System.Timers.ElapsedEventHandler(tim_Elapsed);
            this.MouseLeave += new EventHandler(DataGridCellTips_MouseLeave);
            this.MouseEnter += new EventHandler(DataGridCellTips_MouseEnter);
        }
        void DataGridCellTips_MouseEnter(object sender, EventArgs e)
        {
            toolTip1.Active = true;
        }
        void DataGridCellTips_MouseLeave(object sender, EventArgs e)
        {
            toolTip1.Active = false;
        }
        private DataTable InitializeGrid(Contact[] cnts)
        {
            testData = new DataSet();
            testTable = new DataTable();
            testTable.Columns.Add(new DataColumn("Last Name", typeof(System.String)));
            testTable.Columns.Add(new DataColumn("First Name", typeof(System.String)));
            testTable.Columns.Add(new DataColumn("City", typeof(System.String)));
            testTable.Columns.Add(new DataColumn("State", typeof(System.String)));
            testTable.Columns.Add(new DataColumn("Zip Code", typeof(System.String)));
            for (int i = 0; i < cnts.Length - 1; i++)
            {
                cellTipRow newRow = new cellTipRow(cnts[i]);
                testTable.Rows.Add(newRow.items());
            }
            testData.Tables.Add(testTable);
            testTable.AcceptChanges();
            return testTable;
        }
        public Contact getSelectedContact(int RowPos)
        {
            Contact cn = null;
            String cntl = (string)this.Rows[RowPos].Cells[0].Value;
            String cntf = (string)this.Rows[RowPos].Cells[1].Value;
            for (int i = 0; i < cnt.Length - 1; i++)
            {
                if (cnt[i].LastName == cntl && cnt[i].FirstName == cntf)
                    cn = cnt[i];
            }
            if (cn != null)
                return cn;
            else
                return null;
        }
        private void InitializeControls(DataTable testTable)
        {
            this.DataSource = testTable;
        }
        public void addCnts(Contact[] cnts)
        {
            DataTable ds = InitializeGrid(cnts);
            InitializeControls(ds);
            this.cnt = new Contact[cnts.Length - 1];
            this.cnt = cnts;
        }
        private void tim_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            toolTip1.Active = true;
        }
        private void HandleMouseMove(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = this.HitTest(e.X, e.Y);
            if (hti.RowIndex != hitRow)
            {     //new hit row 
                hitRow = hti.RowIndex;
                if (this.toolTip1 != null && this.toolTip1.Active)
                    this.toolTip1.Active = false; //turn it off
                if (hitRow <= this.Rows.Count - 1 && hitRow >= 0)
                {
                    DataTable dt = (DataTable)this.DataSource;
                    string fName = (string)this.Rows[hitRow].Cells[1].Value;
                    string lName = (string)this.Rows[hitRow].Cells[0].Value;
                    //MessageBox.Show(getNumbers(fName, lName));
                    this.toolTip1.ToolTipTitle = fName + " " + lName;
                    this.toolTip1.SetToolTip(this, getNumbers(fName, lName));
                    //Console.WriteLine("MouseMove "+ hitRow.ToString() + " " + hitCol.ToString()); 
                }
                toolTip1.Active = true;
            }
        }
        public void DeleteContact(int RowIndex, DirectoryInfo f)
        {
            FileInfo[] fi=new FileInfo[f.GetFiles("*.cnt").Length];
            int[] selrws=new int[this.SelectedRows.Count+1];
            for (int i = 0; i < this.SelectedRows.Count; i++)
            {
                selrws[i] = this.SelectedRows[i].Index;
            }
            
            for (int i = 0; i < selrws.Length-1; i++)
            {
                fi = f.GetFiles("*.cnt");
                for(int a=0;a<fi.Length;a++)
                {
                    FileInfo fin = fi[a];
                    string end = (string)this.Rows[selrws[i]].Cells[0].Value;
                    if (fin.FullName.EndsWith((string)this.Rows[selrws[i]].Cells[1].Value + end.Substring(0, 3) + ".cnt"))
                    {
                        fin.Delete();
                        a = fi.Length;
                    }
                }
                this.Rows.RemoveAt(selrws[i]);
            }
        }
        private String getNumbers(string fname, string lname)
        {
            System.Text.StringBuilder info = new System.Text.StringBuilder();
            string tds = null;
            for (int i = 0; i < cnt.Length - 1; i++)
            {
                if (cnt[i].FirstName == fname && cnt[i].LastName == lname)
                {
                    tds = (Phone_Book.Properties.Settings.Default.showHNumber ? "Home #:" + cnt[i].HomeNumber : "") + (Phone_Book.Properties.Settings.Default.showMNumber ? "\n" + "Moble #:" + cnt[i].MobleNumber : "") + (Phone_Book.Properties.Settings.Default.showWNumber ? "\n" + "Work #:" + cnt[i].WorkNumber : "") + (Phone_Book.Properties.Settings.Default.showEmail ? "\n" + "E-mail:   " + cnt[i].EMail : "");
                    //info.Append("Home #:  " + cnt[i].HomeNumber);
                    //info.AppendLine("Moble #:   " + cnt[i].MobleNumber);
                    //info.AppendLine("Work #:   " + cnt[i].WorkNumber);
                    //info.AppendLine("E-mail:   " + cnt[i].EMail);
                    i = cnt.Length;
                }
            }
            if (info == null)
                tds = ("There was an error retreiving data");
            string t = info.ToString();
            return tds;
        }
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
        }
    }
    public class cellTipRow
    {
        private Contact cnt;
        private object[] obj;
        private string[] pop;
        public cellTipRow(Contact cnt)
        {
            this.cnt = cnt;
            applyValues();
            applyPopup();
        }
        private void applyPopup()
        {
            pop = new string[]
        {
            cnt.HomeNumber.ToString(),
            cnt.MobleNumber.ToString(),
            cnt.WorkNumber.ToString(),
            cnt.EMail.ToString()
        };
        }
        private void applyValues()
        {
            this.obj = new object[]
        {
            cnt.LastName,
            cnt.FirstName,
            cnt.City,
            cnt.State,
            cnt.ZipCode
        };
        }
        public object[] items()
        {
            return obj;
        }
    }
}