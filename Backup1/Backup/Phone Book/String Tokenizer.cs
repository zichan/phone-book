using System;
using System.Collections.Generic;
using System.Text;

namespace Phone_Book
{
    public class Token 
    { 
        private string data,delimeter; 
        private string[] tokens; 
        private int index;
        public String City, State, Country, Zip;
        public Token(string String) 
        { 
            init(String,"/"); 
        } 
        private void init(string strdata, string delim) 
        { 
            data = strdata; 
            delimeter = delim; 
            tokens = data.Split(delimeter.ToCharArray()); 
            index = 0;
            City = nextElement();
            State = nextElement();
            Country = nextElement();
            Zip = nextElement();
        }
        public bool hasElements() 
        { 
            return (index < (tokens.Length)); 
        }
        private string nextElement() 
        { 
            if(index < tokens.Length) 
            { 
                return tokens[index++]; 
            } 
            else 
            { 
                return ""; 
            } 
        } 
    } 
}
