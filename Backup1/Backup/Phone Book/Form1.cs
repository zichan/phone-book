using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Permissions;
using System.Security;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
using System.Threading;
using System.Runtime.InteropServices; 
namespace Phone_Book
{
    public partial class Desktop : Form
    {
        static String file;
        Contact[] cnts;
        Contact selectedContact;
        FileInfo[] rgFiles;
        Boolean close = false;
        Search sear;
        int rtd;
        DirectoryInfo di;
        String CurrentDir=string.Empty;
        [STAThread]
        static void Main(String[] args)
        {
            Boolean AlreadyRunning =false;
            System.Diagnostics.Process[] them = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process a in them)
            {
                if (a.ProcessName == "Phone Book.exe")
                {
                    AlreadyRunning = true;
                }
            }
            if (args.Length > 0&&AlreadyRunning==false)
            {
                file = args[0];
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new ContactView(file,true));
            }
            else if(AlreadyRunning==false)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Desktop());
            }
        }
        public Desktop()
        {
            InitializeComponent();
        }
        protected virtual void OnAcceptChanges(System.ComponentModel.CancelEventArgs e)
        {
        }
        private void Desktop_Load(object sender, EventArgs e)
        {
            LoadContacts();
            notifyIcon1.Visible = true;
            timer2.Start();
            for(int i=0;i<cnts.Length-1;i++)
            {
                if(cnts[i].FirstName==(string)dataGrid1.SelectedRows[0].Cells[1].Value&&cnts[i].LastName==(string)dataGrid1.SelectedRows[0].Cells[0].Value)
                {
                    selectedContact=cnts[i];
                }
            }
        }
        public void LoadContacts()
        {
            DirectoryInfo di;
            if (Properties.Settings.Default.PrgmOp == 1)
                di = new DirectoryInfo(Properties.Settings.Default.LastDir+"/");
            else if (Properties.Settings.Default.PrgmOp==2)
                di = new DirectoryInfo(Properties.Settings.Default.UserDir);
            else
            {
                di = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "/Address Book");
                if (di.Exists == false)
                    di.Create();
            }
            CurrentDir=di.FullName;
            Properties.Settings.Default.CurrentDir = di.FullName;
            Properties.Settings.Default.Save();
            rgFiles = di.GetFiles("*.cnt");
            cnts = new Contact[rgFiles.Length + 1];
            if (rgFiles.Length != 0)
            {
                for (int i = 0; i < rgFiles.Length; i++)
                {
                    cnts[i] = load(rgFiles[i]);
                }
                dataGrid1.addCnts(cnts);
            }
            try
            {
                dataGrid1.Sort(dataGrid1.Columns[0], ListSortDirection.Ascending);
                int siz = 0;
                foreach (DataGridViewColumn a in dataGrid1.Columns)
                {
                    siz += a.Width;
                }
                dataGrid1.Width = siz+20;
                this.Width = siz + 20;
            }
            catch
            { }
        }
        public void LoadContacts(String FolderPath)
        {
            di = new DirectoryInfo(FolderPath);
            rgFiles = di.GetFiles("*.cnt");
            cnts = new Contact[rgFiles.Length + 1];
            if (rgFiles.Length != 0)
            {
                for (int i = 0; i < rgFiles.Length; i++)
                {
                    cnts[i] = load(rgFiles[i]);
                }
                dataGrid1.addCnts(cnts);
            } 
            try
            {
                dataGrid1.Sort(dataGrid1.Columns[0], ListSortDirection.Ascending);
                int siz = 0;
                foreach (DataGridViewColumn a in dataGrid1.Columns)
                {
                    siz += a.Width;
                }
                dataGrid1.Width = siz + 20;
                this.Width = siz + 25;
            }
            catch
            { }
        }
        public Contact load(FileInfo file)
        {
            Contact cnt;
            XmlSerializer s = new XmlSerializer(typeof(Contact));
            TextReader w = new StreamReader(file.FullName);
            cnt = (Contact)s.Deserialize(w);
            w.Close();
            return cnt;
        }
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            String filename = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            Open.InitialDirectory = filename;
            if (Open.ShowDialog() == DialogResult.OK)
            {
                foreach (string s in Open.FileNames)
                {
                }
            }
        }
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ContData Cnt = new ContData();
            Cnt.ShowDialog(this);
        }
        private void DeleteB_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Shure You Want To Delete This Contact/s?", "Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int a = dataGrid1.SelectedRows[0].Index;
                dataGrid1.DeleteContact(rtd,rgFiles[0].Directory);
                if(a>0&&a<dataGrid1.Rows.Count)
                    dataGrid1.Rows[a].Selected = true;
                try
                {
                    dataGrid1.Sort(dataGrid1.Columns[0], ListSortDirection.Ascending);
                    int siz = 0;
                    foreach (DataGridViewColumn c in dataGrid1.Columns)
                    {
                        siz += c.Width;
                    }
                    dataGrid1.Width = siz + 20;
                    this.Width = siz + 20;
                }
                catch { }
            }
        }
        private void dataGrid1_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo hti = dataGrid1.HitTest(e.X, e.Y);
            if (hti.RowIndex >= 0)
            {
                selectedContact = dataGrid1.getSelectedContact(hti.RowIndex);
                switch (e.Button)
                {
                    case MouseButtons.Right:

                        if (hti.RowIndex >= 0 && hti.RowIndex < dataGrid1.Rows.Count && dataGrid1.SelectedRows.Count == 1)
                        {
                            dataGrid1.SelectedRows[0].Selected = false;
                            dataGrid1.Rows[hti.RowIndex].Selected = true;
                        }
                        rtd = hti.RowIndex;
                        contextMenuStrip1.Show(dataGrid1, e.X, e.Y);
                        break;
                }
            }
        }
        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Options op = new Options();
            op.ShowDialog(this);
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Open.ShowDialog(this) == DialogResult.OK)
            {
                FileInfo fi = new FileInfo(Open.FileName);
                if (Properties.Settings.Default.OPcnt == 1)
                {
                    new ContactView(fi.FullName, false).Show(this);
                }
                else if (Properties.Settings.Default.OPcnt == 2)
                {
                    fi.CopyTo(CurrentDir);
                    new ContactView(fi.FullName, false).Show(this);
                }
                else
                {
                    fi.CopyTo(CurrentDir);
                }
            }
        }
        private void dataGrid1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string filename;
            if (e.RowIndex < dataGrid1.Rows.Count-1 && e.RowIndex >= 0)
            {
                string Fname = (string)dataGrid1.Rows[e.RowIndex].Cells[1].Value;
                string Lname = (string)dataGrid1.Rows[e.RowIndex].Cells[0].Value;
                string name = Fname + Lname.Substring(0, 3) + ".cnt";
                for (int i = 0; i < rgFiles.Length; i++)
                {
                    if (rgFiles[i].FullName.EndsWith(name))
                    {
                        filename = rgFiles[i].FullName;
                        ContactView cnv = new ContactView(filename, false);
                        cnv.Owner = this;
                        cnv.ShowDialog(this);
                    }
                }
            }
        }
        private void EditB_Click(object sender, EventArgs e)
        {
            Contact cn = null;
            FileInfo fi= null;
            string Lname = (string)dataGrid1.Rows[rtd].Cells[0].Value;
            String name = (string)dataGrid1.Rows[rtd].Cells[1].Value + Lname.Substring(0, 3) + ".cnt";
            foreach(FileInfo fin in rgFiles)
            {
                if(fin.FullName.EndsWith(name))
                {
                    fi=fin;
                }
            }
            cn = load(fi);
            ContData cdt = new ContData(cn,fi);
            cdt.ShowDialog(this);
        }
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Backup bkp = new Backup();
            bkp.Contacts = cnts;
            StatLabel.Visible = true;
            StatLabel.Text = "Restoring";
            this.Refresh();
            dataGrid1.Update();
            SAve(bkp);
            Properties.Settings.Default.LastUpdate = DateTime.Now;
            Phone_Book.Properties.Settings.Default.Save();
            StatLabel.Visible = false;
            StatLabel.Text = "";
        }
        private void toolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            OpenRestore.InitialDirectory = Application.StartupPath + "/BackUp Files/";
            OpenRestore.ShowDialog(this);
        }
        private void OpenRestore_FileOk(object sender, CancelEventArgs e)
        {
            String file=null;
            String FileName = OpenRestore.FileName;
            if (folderBrowserDialog1.ShowDialog(this) == DialogResult.OK)
                file = folderBrowserDialog1.SelectedPath+"/";
            else
                return;
            Backup bk = Restore(FileName);
            Contact[] cs = bk.Contacts;
            toolStripProgressBar1.Visible = true;
            StatLabel.Visible = true;
            StatLabel.Text = "Restoring";
            this.Refresh();
            toolStripProgressBar1.Minimum = 0;
            toolStripProgressBar1.Maximum = cs.Length;
            toolStripProgressBar1.Value = 1;
            toolStripProgressBar1.Step = 1;
            if (Properties.Settings.Default.BkpWrit == 1)
            {
                DirectoryInfo di = new DirectoryInfo(file);
                FileInfo[] fi = di.GetFiles("*.cnt");
                for (int i = 0; i < fi.Length; i++)
                    fi[i].Delete();
                for (int i = 1; i < cs.Length; i++)
                {
                    cs[i - 1].Save(file);
                    toolStripProgressBar1.PerformStep();
                }
            }
            if (Properties.Settings.Default.BkpWrit == 2)
            {
                for (int i = 1; i < cs.Length; i++)
                {
                    FileInfo fi = new FileInfo(file + cs[i].FirstName + cs[i].LastName.Substring(0, 3) + ".cnt");
                    if (fi.Exists)
                        fi.Delete();
                    cs[i - 1].Save(file);
                    toolStripProgressBar1.PerformStep();
                }
            }
            if (Properties.Settings.Default.BkpWrit == 3)
            {
                DirectoryInfo di = new DirectoryInfo(file);
                FileInfo[] fi = di.GetFiles();
                for (int i = 0; i < fi.Length; i++)
                {
                    fi[i].MoveTo(Properties.Settings.Default.MoveCnt+fi[i].Name);
                }
                for (int i = 1; i < cs.Length; i++)
                {
                    cs[i - 1].Save(file);
                    toolStripProgressBar1.PerformStep();
                }
            }
            StatLabel.Visible = false;
            StatLabel.Text = "";
            toolStripProgressBar1.Visible = false;
            toolStripProgressBar1.Value = 0;
            LoadContacts();
        }
        public Boolean saveFile(Contact ct)
        {
            Boolean result;
            try
            {
                ct.Save();
                result=true;
            }
            catch (Exception)
            {
                result= false;
            }
            return result;
        }
        public Backup Restore(String FilePath)
        {
            Backup bkp;
            XmlSerializer s = new XmlSerializer(typeof(Backup));
            TextReader w = new StreamReader(FilePath);
            bkp = (Backup)s.Deserialize(w);
            w.Close();
            return bkp;
        }
        void SAve(Backup bk)
        {
            XmlSerializer s = new XmlSerializer(typeof(Backup));
            FileStream w = null;
            MemoryStream memstrm = new MemoryStream();
            // Wrap memstrm in a reader and a writer. 
            StreamWriter memwtr = new StreamWriter(memstrm);
            DateTime dt = DateTime.Now;
            DirectoryInfo d = new DirectoryInfo(Application.StartupPath + "/BackUp Files/");
            if (d.Exists == false)
                d.Create();
            if (Phone_Book.Properties.Settings.Default.BkpSv == 1)
            {
                w = new FileStream(Application.StartupPath + "/BackUp Files/" + dt.Day.ToString() + dt.Month.ToString() + dt.Year.ToString() + ".bkp", FileMode.Create, FileAccess.ReadWrite);
            }
            else
            {
                FolderBrowserDialog f = new FolderBrowserDialog();
                f.ShowNewFolderButton = true;
                if (f.ShowDialog() == DialogResult.OK)
                    w = new FileStream(f.SelectedPath + dt.Day.ToString() + dt.Month.ToString() + dt.Year.ToString() + ".bkp", FileMode.Create, FileAccess.ReadWrite);
            }
            s.Serialize(memwtr, bk);
            byte[] files = memstrm.ToArray();
            toolStripProgressBar1.Visible = true;
            toolStripProgressBar1.Minimum = 0;
            toolStripProgressBar1.Maximum = 10;
            toolStripProgressBar1.Value = 1;
            toolStripProgressBar1.Step = 1;
            int left = (int)decimal.Round(decimal.Remainder((decimal)files.Length / 10, 10));
            int left1 = 0;
            for (int i = 0; i <= 9; i++)
            {
                w.Write(files,i*(files.Length/10),files.Length/10);
                toolStripProgressBar1.PerformStep();
                left1 = i*(files.Length/10)+files.Length/10;
            }
            if (left != 0)
                w.Write(files, left1,files.Length-left1);
            toolStripProgressBar1.Value = 0;
            toolStripProgressBar1.Visible = false;
            memstrm.Close();
            w.Close();
        }
        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sear == null)
            {
                sear = new Search(cnts, this);
                sear.Location = new Point(this.Location.X + this.Size.Width, this.Location.Y);
                sear.Show(this);
            }
            else if (sear.closed == true)
            {
                sear = new Search(cnts, this);
                sear.Location = new Point(this.Location.X + this.Size.Width, this.Location.Y);
                sear.Show(this);
                sear.closed = false;
            }
            else
                sear.Select();
        }
        private void Desktop_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (close == false&&Properties.Settings.Default.MinimTray==true)
            {
                Hide();
                e.Cancel = true;
            }
        }
        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            close = true;
            Application.Exit();
        }
        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Show();
        }
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ContData cnt = new ContData();
            cnt.StartPosition = FormStartPosition.CenterScreen;
            cnt.Show();
        }
        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyDocuments;
            if (folderBrowserDialog1.ShowDialog(this) == DialogResult.OK)
            {
                LoadContacts(folderBrowserDialog1.SelectedPath);
                if (Properties.Settings.Default.PrgmOp == 1)
                {
                    Properties.Settings.Default.LastDir = folderBrowserDialog1.SelectedPath + "/";
                    Properties.Settings.Default.Save();
                }
            }
            this.dataGrid1.Refresh();
        }
        private void Desktop_LocationChanged(object sender, EventArgs e)
        {
            if (sear != null)
                sear.Location = new Point(this.Location.X + this.Size.Width, this.Location.Y);
        }
        private void timer2_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            DateTime dt = DateTime.Now;
            DateTime dt1 = Phone_Book.Properties.Settings.Default.LastUpdate;
            int time;
            if (dt.Year == dt1.Year)
            {
                if (dt.Month == dt1.Month)
                {
                    time = dt.Day - dt1.Day;
                }
                else
                {
                    int dif = dt.Month - dt1.Month;
                    int days = DateTime.DaysInMonth(dt1.Year, dt1.Month) - dt1.Day;
                    if (dif > 1)
                    {
                        time = dt.Day;
                        for (int i = 0; i < dif-1; i++)
                        {
                            time += DateTime.DaysInMonth(dt1.Year, dt1.Month + (i+1));
                        }
                        time += days;
                    }
                    else
                    {
                        time = days;
                        days += dt.Day;
                    }
                }
            }
            else
            {
                time = 0;
            }
            if (time > Phone_Book.Properties.Settings.Default.BkpInterval)
            {
                notifyIcon1.ShowBalloonTip(4000, "BackUp", "The Alloted Time That You Set Has Passed Since Your Last BackUp." + "\n" + "Click Here To Perform BackUp:", ToolTipIcon.Info);
                this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClickedBackUp);
            }
        }
        private void notifyIcon1_BalloonTipClickedBackUp(object sender, EventArgs e)
        {
            Backup bkp = new Backup();
            bkp.Contacts = cnts;
            this.Invalidate(true);
            SAve(bkp);
            Properties.Settings.Default.LastUpdate = DateTime.Now;
            Phone_Book.Properties.Settings.Default.Save();
        }
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog(this) == DialogResult.OK)
            {
                toolStripProgressBar1.Visible = true;
                toolStripProgressBar1.Minimum = 0;
                toolStripProgressBar1.Maximum = cnts.Length-2;
                toolStripProgressBar1.Value = 1;
                toolStripProgressBar1.Step = 1;
                for (int i = 0; i < cnts.Length-1; i++)
                {
                    cnts[i].Save(folderBrowserDialog1.SelectedPath+"/");
                    toolStripProgressBar1.PerformStep();
                } 
                toolStripProgressBar1.Visible = false;
            }
        }
        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printDialog1.Document = printDocument1;
            printDialog1.ShowDialog();
        }
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            Font myFont = new Font("Arial", 10);
            g.DrawString("First Name:  "+selectedContact.FirstName, myFont, Brushes.Black, 10, 10);
            g.DrawString("Last Name:  " + selectedContact.LastName, myFont, Brushes.Black, 10, 25);
            g.DrawString("Address:  " + selectedContact.Address, myFont, Brushes.Black, 10, 40);
            g.DrawString("City:  " + selectedContact.City, myFont, Brushes.Black, 10, 55);
            g.DrawString("Zip Code:  " + selectedContact.ZipCode, myFont, Brushes.Black, 10, 70);
            g.DrawString("Country/Region:  " + selectedContact.Country, myFont, Brushes.Black, 10, 85);
            if (selectedContact.Picture != null) 
                g.DrawImage(selectedContact.Picture, 10, 100);
        }
        private void printPreviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.ShowDialog();
        }
        private void Desktop_Enter(object sender, EventArgs e)
        {
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new About().ShowDialog(this);
        }
    }
}