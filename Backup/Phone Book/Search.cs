using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Phone_Book
{
    public partial class Search : Form
    {
        RadioButton last = null;
        int lastHit = 0;
        int phSearch = 0;
        Contact[] contacts;
        int timerPos = 0;
        Desktop dsk;
        double OPaq=1d;
        public Boolean closed = false;
        public Search(Contact[] cts,Desktop dk)
        {
            dsk = dk;
            contacts = cts;
            InitializeComponent();
        }
        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                ButtonStrip1.Show(checkBox3, checkBox3.Size.Width/2, checkBox3.Size.Height/2);
                if (radioButton1.Checked)
                    radioButton1.Checked = false;
                else if (radioButton2.Checked)
                    radioButton2.Checked = false;
                else if (radioButton3.Checked)
                    radioButton3.Checked = false;
                else if (radioButton4.Checked)
                    radioButton4.Checked = false;
            }
            else
            {
                checkBox3.Text = "Phone Number";
                if (last != null)
                    last.Checked = true;
            }
        }
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            phSearch = 1;
            checkBox3.Text = "Home Number";
        }
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            phSearch = 2;
            checkBox3.Text = "Work Number";
        }
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            phSearch = 3;
            checkBox3.Text = "Moble Number";
        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true && checkBox3.Checked == true)
                checkBox3.Checked = false;
        }
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true && checkBox3.Checked == true)
                checkBox3.Checked = false;
        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked == true && checkBox3.Checked == true)
                checkBox3.Checked = false;
        }
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked == true && checkBox3.Checked == true)
                checkBox3.Checked = false;
        }
        private Boolean SearchSelect()
        {
            Boolean result = true;
            if (result == false)
                result = radioButton1.Checked;
            if (result == false)
                result = radioButton2.Checked;
            if (result == false)
                result = radioButton3.Checked;
            if (result == false)
                result = radioButton4.Checked;
            if (result == false && phSearch != 0)
                result = true;
            return result;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (SearchBox.Text == "")
                MessageBox.Show("Please Enter Search Criteria");
            else
            {
                if (radioButton1.Checked)
                    searchFName();
                else if (radioButton2.Checked)
                    searchLName();
                else if (radioButton3.Checked)
                    searchEmail();
                else if (radioButton4.Checked)
                    searchMailaddr();
                else if (phSearch != 0)
                    searchPHNumber();
            }
        }
        private void searchFName()
        {
            DataGridCellTips ct = dsk.dataGrid1;
            for (int i = lastHit; i < ct.Rows.Count - 1; i++)
            {
                if ((string)ct.Rows[i].Cells[1].Value == SearchBox.Text)
                {
                    ct.Rows[i].Selected = true;
                    lastHit = i+1;
                    i = ct.Rows.Count - 1;
                }
                else if (i == ct.Rows.Count-2)
                {
                    MessageBox.Show(this, "There was no contact found with your search query", "None Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lastHit = 0;
                }
            }
        }
        private void searchLName()
        {
            DataGridCellTips ct = dsk.dataGrid1;
            for (int i = lastHit; i < ct.Rows.Count - 1; i++)
            {
                if ((string)ct.Rows[i].Cells[0].Value == SearchBox.Text)
                {
                    ct.Rows[i].Selected = true;
                    lastHit = i+1;
                    i = ct.Rows.Count - 1;
                }
                else if (i == ct.Rows.Count-2)
                {
                    MessageBox.Show(this, "There was no contact found with your search query", "None Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lastHit = 0;
                }
            }
        }
        private void searchPHNumber()
        {
            DataGridView ct = dsk.dataGrid1;
            for(int i=lastHit;i<ct.Rows.Count;i++)
            {
                foreach(Contact crt in contacts)
                {
                    if (crt != null)
                    {
                        if ((string)ct.Rows[i].Cells[1].Value == crt.FirstName && (string)ct.Rows[i].Cells[0].Value == crt.LastName)
                        {
                            if (phSearch == 1)
                            {
                                if (crt.HomeNumber == SearchBox.Text)
                                {
                                    ct.Rows[i].Selected = true;
                                    lastHit = i + 1;
                                    i = ct.Rows.Count - 1;
                                }
                            }
                            if (phSearch == 2)
                            {
                                if (crt.WorkNumber == SearchBox.Text)
                                {
                                    ct.Rows[i].Selected = true;
                                    lastHit = i + 1;
                                    i = ct.Rows.Count - 1;
                                }
                            }
                            if (phSearch == 3)
                            {
                                if (crt.MobleNumber == SearchBox.Text)
                                {
                                    ct.Rows[i].Selected = true;
                                    lastHit = i + 1;
                                    i = ct.Rows.Count - 1;
                                }
                            }
                        }
                    }
                }
                if(i==ct.Rows.Count-2)
                {
                    MessageBox.Show(this, "There was no contact found with your search query", "None Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lastHit = 0;
                }
            }
        }
        private void searchEmail()
        {
            DataGridCellTips ct = dsk.dataGrid1;
            for (int i = lastHit; i < ct.Rows.Count - 1; i++)
            {
                if ((string)ct.Rows[i].Cells[1].Value == SearchBox.Text)
                {
                    ct.Rows[i].Selected = true;
                    lastHit = i + 1;
                    i = ct.Rows.Count - 1;
                }
                else if (i == ct.Rows.Count - 2)
                {
                    MessageBox.Show(this, "There was no contact found with your search query", "None Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lastHit = 0;
                }
            }
        }
        private void searchMailaddr()
        {
            DataGridCellTips ct = dsk.dataGrid1;
            for (int i = lastHit; i < ct.Rows.Count - 1; i++)
            {
                if ((string)ct.Rows[i].Cells[1].Value == SearchBox.Text)
                {
                    ct.Rows[i].Selected = true;
                    lastHit = i + 1;
                    i = ct.Rows.Count - 1;
                }
                else if (i == ct.Rows.Count - 2)
                {
                    MessageBox.Show(this, "There was no contact found with your search query", "None Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lastHit = 0;
                }
            }
        }
        Boolean opening = true;
        double a = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            Size = new Size((int)Math.Sqrt(Math.Pow(300,2)-Math.Pow(timerPos-300,2)),250);
            a = (double)timerPos / 300;
            timerPos+=25;
            if (opening==true)
                Opacity = a;
            if (opening==false)
                Opacity = OPaq -= .06d;
            if (Size.Width == 300 && opening == true)
            {
                Opacity = 1;
                timer1.Stop();
                opening = false;
            }
            if (timerPos==600&&opening==false)
            {
                timer1.Stop();
                this.Hide();
            }
        }
        private void Search_Load(object sender, EventArgs e)
        {
            Size = new Size(0, 250);
            timer1.Start();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            closed = true;
            timer1.Start();
        }
        private void radioButton1_Click(object sender, EventArgs e)
        {
            SearchBox.Focus();
            if(sender.GetType().Equals((typeof(RadioButton))))
            {
                last=(RadioButton)sender;
            }
        }
    }
}