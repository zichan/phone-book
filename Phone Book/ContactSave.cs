using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;  // Does XML serializing for a class.
using System.Drawing;            // Required for storing a Bitmap.
using System.IO;			     // Required for using Memory stream objects.
using System.ComponentModel;     // Required for conversion of Bitmap objects.

namespace Phone_Book
{
    public class Customer
    {
        public string LName;
        public string FName;
        public string address;
        public string city;
        public string eMail;
        public int zCode;
        public string hNumber;
        public string wNumber;
        public string mNumber;
        public string notes;
        private Bitmap picture;
        public Customer()
        {
        }
        [XmlIgnoreAttribute()]
        public Bitmap Picture
        {
            get { return picture; }
            set { picture = value; }
        }
        #region XML Elements and variable thingy's
        [XmlElement("Moble Number")]
        public String MobleNumber
        {
            get
            {
                return mNumber;
            }
            set
            {
                mNumber = value;
            }
        }
        [XmlElement("Work Number")]
        public String WorkNumber
        {
            get
            {
                return wNumber;
            }
            set
            {
                wNumber = value;
            }
        }
        [XmlElement("Home Number")]
        public String HomeNumber
        {
            get
            {
                return hNumber;
            }
            set
            {
                hNumber = value;
            }
        }
        [XmlElement("Zip Code")]
        public int ZipCode
        {
            get
            {
                return zCode;
            }
            set
            {
                zCode = value;
            }
        }
        [XmlElement("Email")]
        public String EMail
        {
            get
            {
                return eMail;
            }
            set
            {
                eMail = value;
            }
        }
        [XmlElement("City")]
        public String City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }
        [XmlElement("Address")]
        public String Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }
        [XmlElement("Last Name")]
        public String LastName
        {
            get
            {
                return LName;
            }
            set
            {
                LName = value;
            }
        }
        [XmlElement("First Name")]
        public String FirstName
        {
            get
            {
                return FName;
            }
            set
            {
                FName = value;
            }
        }
        [XmlElement("Notes")]
        public String Notes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }
        [XmlElementAttribute("Picture")]
        public byte[] PictureByteArray
        {
            get
            {
                if (picture != null)
                {
                    TypeConverter BitmapConverter = TypeDescriptor.GetConverter(picture.GetType());
                    return (byte[])BitmapConverter.ConvertTo(picture, typeof(byte[]));
                }
                else
                    return null;
            }

            set
            {
                if (value != null)
                    picture = new Bitmap(new MemoryStream(value));
                else
                    picture = null;
            }
        }
        #endregion
        public void Save()
        {
            XmlSerializer s = new XmlSerializer(typeof(Contact));
            //Stream stream = System.IO.File.Open(cnt.FirstName+".cnt", FileMode.CreateNew);
            string Lnamef3 = LName.ToCharArray(0, 2).ToString();
            TextWriter w = new StreamWriter(this.FirstName + Lnamef3 + ".cnt");
            s.Serialize(w, this);
            w.Close();
        }
        // Serializes an ArrayList as a "Hobbies" array of XML elements of type string named "Hobby".
        [XmlArray("Hobbies"), XmlArrayItem("Hobby", typeof(string))]
        public System.Collections.ArrayList Hobbies = new System.Collections.ArrayList();
    }
}