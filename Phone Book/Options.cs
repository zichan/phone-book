using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
namespace Phone_Book
{
    public partial class Options : Form
    {
        public Options()
        {
            InitializeComponent();
        }           
        private void Options_Load(object sender, EventArgs e)
        {
            if (Phone_Book.Properties.Settings.Default.BkpReminder == 1)
            {
                radioButton3.Checked = true;
                if (Properties.Settings.Default.BkpInterval >= 7)
                {
                    int s = Properties.Settings.Default.BkpInterval / 7;
                    textBox1.Text = s.ToString();
                    listBox1.SelectedIndex = 1;
                }
                else
                    textBox1.Text = Properties.Settings.Default.BkpInterval.ToString();
            }
            else
                radioButton7.Checked = true;
            if (Properties.Settings.Default.OPcnt == 1)
                radioButton5.Checked = true;
            else if (Properties.Settings.Default.OPcnt == 2)
                radioButton4.Checked = true;
            else
                radioButton6.Checked = true;
            checkBox1.Checked = Properties.Settings.Default.showHNumber;
            checkBox2.Checked = Properties.Settings.Default.showWNumber;
            checkBox3.Checked = Properties.Settings.Default.showMNumber;
            checkBox4.Checked = Properties.Settings.Default.showEmail;
            if (Properties.Settings.Default.SaveSet == 1)
                radioButton1.Checked = true;
            else if (Properties.Settings.Default.SaveSet == 2)
                radioButton2.Checked = true;
            if (Properties.Settings.Default.PrgmOp == 1)
                prgmo1.Checked = true;
            else if (Properties.Settings.Default.PrgmOp == 2)
                prgmo2.Checked = true;
            else
                prgmo3.Checked = true;
            if (Properties.Settings.Default.BkpSv == 1)
                bksv1.Checked = true;
            else
                bksv1.Checked = true;
            if (Properties.Settings.Default.BkpWrit == 1)
                bkres1.Checked = true;
            else if (Properties.Settings.Default.BkpWrit == 2)
                bkres2.Checked = true;
            else
                bkres3.Checked = true;
            checkBox5.Checked = Properties.Settings.Default.MinimTray;
            folderBrowserDialog1.SelectedPath = Properties.Settings.Default.SaveLoc;
            folderBrowserDialog2.SelectedPath = Properties.Settings.Default.UserDir;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Phone_Book.Properties.Settings.Default.BkpReminder = radioButton3.Checked ? 1 : radioButton7.Checked ? 2 : 2;
            if (listBox1.SelectedIndex == 0)
                Phone_Book.Properties.Settings.Default.BkpInterval = int.Parse(textBox1.Text);
            else
            {
                int a = int.Parse(textBox1.Text) * 7;
                Phone_Book.Properties.Settings.Default.BkpInterval = a;
            }
            Phone_Book.Properties.Settings.Default.showHNumber = checkBox1.Checked;
            Phone_Book.Properties.Settings.Default.showWNumber = checkBox2.Checked;
            Phone_Book.Properties.Settings.Default.showMNumber = checkBox3.Checked;
            Phone_Book.Properties.Settings.Default.showEmail = checkBox4.Checked;
            Phone_Book.Properties.Settings.Default.SaveSet = (radioButton1.Checked ? 1 : (radioButton2.Checked ? 2 :1));
            Phone_Book.Properties.Settings.Default.PrgmOp = (prgmo1.Checked ? 1 : (prgmo2.Checked ? 2 : (prgmo3.Checked ? 3 : 3)));
            Phone_Book.Properties.Settings.Default.BkpSv = (bksv1.Checked ? 1:(bksv2.Checked ? 2:1));
            Phone_Book.Properties.Settings.Default.BkpWrit = (bkres1.Checked ? 1 : (bkres2.Checked ? 2 : (bkres3.Checked ? 3 : 1)));
            Properties.Settings.Default.MinimTray = checkBox5.Checked;
            Properties.Settings.Default.OPcnt = radioButton5.Checked ? 1 : radioButton4.Checked ? 2 : radioButton6.Checked ? 3 : 1;
            Properties.Settings.Default.BkpReminder = radioButton3.Checked ? 1 : 2;
            //Properties.Settings.Default.BkpInterval = listBox1.SelectedIndex <= 6 ? listBox1.SelectedIndex + 1 : 14;
            Phone_Book.Properties.Settings.Default.Save();
            this.Close();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog2.ShowDialog(this) == DialogResult.OK)
                Properties.Settings.Default.UserDir = folderBrowserDialog2.SelectedPath;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void prgmo2_CheckedChanged(object sender, EventArgs e)
        {
            if (prgmo2.Checked== true)
            {
                if (Properties.Settings.Default.UserDir == "")
                {
                    MessageBox.Show(this, "Please Select A Directory Location");
                    if (folderBrowserDialog2.ShowDialog(this) == DialogResult.OK)
                        Properties.Settings.Default.UserDir = folderBrowserDialog2.SelectedPath;
                    else
                    {
                        if (Properties.Settings.Default.PrgmOp == 1)
                            prgmo1.Checked = true;
                        else if (Properties.Settings.Default.PrgmOp == 2)
                            prgmo2.Checked = true;
                        else
                            prgmo3.Checked = true;
                    }
                }
            }
        }
        private void bkres3_CheckedChanged(object sender, EventArgs e)
        {
            if (bkres3.Checked == true && Properties.Settings.Default.MoveCnt == "")
            {
                if (folderBrowserDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    Properties.Settings.Default.MoveCnt = folderBrowserDialog1.SelectedPath;
                }
                else
                {
                    if (Properties.Settings.Default.BkpWrit == 1)
                        bkres1.Checked = true;
                    else if (Properties.Settings.Default.BkpWrit == 2)
                        bkres2.Checked = true;
                    else
                        bkres3.Checked = true;
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog(this) == DialogResult.OK)
            {
                Properties.Settings.Default.MoveCnt = folderBrowserDialog1.SelectedPath+"/";
            }
        }
        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton7.Checked == true)
            {
                textBox1.Enabled = false;
                listBox1.Enabled = false;
            }
        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked == true)
            {
                textBox1.Enabled = true;
                listBox1.Enabled = true;
            }
        }
    }
}